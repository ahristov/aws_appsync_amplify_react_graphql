# README #

Contains code and notes from studying [AWS AppSync & Amplify with React & GraphQL - Complete Guide](https://www.udemy.com/course/aws-appsync-amplify-with-react-graphql-course/).

Run the React app with:

```bash
cd reactblog
npm start
```

## AWS Amplify ##

Install `AWS Amplify` with:

```bash
npm install -g @aws-amplify/cli
```

Then configure:

```bash
amplify configure
```

## React app ##

To remove outdated `create-react-app` template:

```bash
npm uninstall -g create-react-app
```

To create the React app:

```bash
npx create-react-app reactblog
```

Run the React app with:

```bash
cd reactblog
npm start
```

Add amplify to react project:

```bash
cd reactblog
amplify init

...
PS D:\Projects\Atanas\aws_appsync_amplify_react_graphql\reactblog> amplify init   
Note: It is recommended to run this command from the root of your app directory
? Enter a name for the project reactblog
The following configuration will be applied:

Project information
| Name: reactblog
| Environment: dev
| Default editor: Visual Studio Code
| App type: javascript
| Javascript framework: react
| Source Directory Path: src
| Distribution Directory Path: build
| Build Command: npm.cmd run-script build
| Start Command: npm.cmd run-script start

? Initialize the project with the above configuration? Yes
Using default provider  awscloudformation
? Select the authentication method you want to use: AWS profile

For more information on AWS Profiles, see:
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html

? Please choose the profile you want to use default
Adding backend environment dev to AWS Amplify app: d21e4y2zftvi6r
\ Initializing project in the cloud...

CREATE_IN_PROGRESS amplify-reactblog-dev-224811 AWS::CloudFormation::Stack Wed Dec 22 2021 22:48:14 GMT-0600 (Central Standard Time) User Initiated
CREATE_IN_PROGRESS UnauthRole                   AWS::IAM::Role             Wed Dec 22 2021 22:48:17 GMT-0600 (Central Standard Time)
CREATE_IN_PROGRESS DeploymentBucket             AWS::S3::Bucket            Wed Dec 22 2021 22:48:17 GMT-0600 (Central Standard Time)
CREATE_IN_PROGRESS AuthRole                     AWS::IAM::Role             Wed Dec 22 2021 22:48:17 GMT-0600 (Central Standard Time)
CREATE_IN_PROGRESS AuthRole                     AWS::IAM::Role             Wed Dec 22 2021 22:48:18 GMT-0600 (Central Standard Time) Resource creation Initiated
CREATE_IN_PROGRESS UnauthRole                   AWS::IAM::Role             Wed Dec 22 2021 22:48:18 GMT-0600 (Central Standard Time) Resource creation Initiated
CREATE_IN_PROGRESS DeploymentBucket             AWS::S3::Bucket            Wed Dec 22 2021 22:48:18 GMT-0600 (Central Standard Time) Resource creation Initiated
| Initializing project in the cloud...

CREATE_COMPLETE AuthRole AWS::IAM::Role Wed Dec 22 2021 22:48:29 GMT-0600 (Central Standard Time) 
- Initializing project in the cloud...

CREATE_COMPLETE UnauthRole AWS::IAM::Role Wed Dec 22 2021 22:48:30 GMT-0600 (Central Standard Time) 
- Initializing project in the cloud...

CREATE_COMPLETE DeploymentBucket AWS::S3::Bucket Wed Dec 22 2021 22:48:39 GMT-0600 (Central Standard Time)
/ Initializing project in the cloud...

CREATE_COMPLETE amplify-reactblog-dev-224811 AWS::CloudFormation::Stack Wed Dec 22 2021 22:48:41 GMT-0600 (Central Standard Time) 
√ Successfully created initial AWS cloud resources for deployments.
√ Initialized provider successfully.
Initialized your environment successfully.

Your project has been successfully initialized and connected to the cloud!

Some next steps:
"amplify status" will show you what you've added already and if it's locally configured or deployed
"amplify add <category>" will allow you to add features like user login or a backend API
"amplify push" will build all your local backend resources and provision it in the cloud
"amplify console" to open the Amplify Console and view your project status
"amplify publish" will build all your local backend and frontend resources (if you have hosting category added) and provision it in the cloud

Pro tip:
Try "amplify add api" to create a backend API and then "amplify publish" to deploy everything
```

## Links ##

[AWS AppSync](https://aws.amazon.com/appsync/)

[AWS AppSync Resolver Mapping](https://docs.aws.amazon.com/appsync/latest/devguide/resolver-mapping-template-reference.html)

[Velocity Template Language](http://people.apache.org/~henning/velocity/html/ch02s02.html)

[AWS Amplify](https://aws.amazon.com/amplify/)

[AWS Amplify GitHub](https://github.com/aws-amplify)

[AWS Amplify Documentation](https://docs.amplify.aws/)

[AWS Amplify CLI](https://docs.amplify.aws/cli/)

[Amplify Flutter](https://pub.dev/packages/amplify_flutter/example)

[Amplify Flutter DataStore](https://pub.dev/packages/amplify_datastore)
